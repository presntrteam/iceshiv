-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 26, 2017 at 06:16 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shivshakti`
--

-- --------------------------------------------------------

--
-- Table structure for table `billdetail`
--

CREATE TABLE `billdetail` (
  `billdetailid` int(2) NOT NULL,
  `billstart` int(10) NOT NULL,
  `vat` int(10) NOT NULL,
  `othertax` int(10) NOT NULL,
  `tinno` varchar(100) NOT NULL,
  `stno` varchar(100) NOT NULL,
  `tax1_label` varchar(20) NOT NULL,
  `tax2_label` varchar(20) NOT NULL,
  `tax3_label` varchar(20) NOT NULL,
  `tax4_label` varchar(20) NOT NULL,
  `tax3_value` int(11) NOT NULL,
  `tax4_value` int(11) NOT NULL,
  `billstartval` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billdetail`
--

INSERT INTO `billdetail` (`billdetailid`, `billstart`, `vat`, `othertax`, `tinno`, `stno`, `tax1_label`, `tax2_label`, `tax3_label`, `tax4_label`, `tax3_value`, `tax4_value`, `billstartval`) VALUES
(1, 1, 10, 5, '123456', '654321', 'tax 1', 'tax 2', 'tax 3', 'tax 4', 5, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bill_item_master`
--

CREATE TABLE `bill_item_master` (
  `bim_id` int(10) NOT NULL,
  `billid` int(10) NOT NULL,
  `item_waiter` int(11) DEFAULT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` int(11) NOT NULL,
  `order_time` varchar(255) NOT NULL,
  `user_by` varchar(255) NOT NULL,
  `parcel` enum('0','1') NOT NULL,
  `weight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_item_master`
--

INSERT INTO `bill_item_master` (`bim_id`, `billid`, `item_waiter`, `bim_itemid`, `bim_item_quantity`, `bim_item_quantity_price`, `bim_item_price`, `order_time`, `user_by`, `parcel`, `weight`) VALUES
(1, 2, 1, 1, 5, 40, 200, '02:42:42', 'admin', '1', 0),
(2, 2, 2, 33, 2, 30, 60, '02:43:09', 'admin', '0', 0),
(3, 2, 2, 1, 1, 400, 100, '14:43:17', 'admin', '0', 250),
(4, 2, 2, 1, 1, 400, 200, '14:43:22', 'admin', '0', 500),
(5, 2, 2, 1, 1, 400, 300, '14:43:26', 'admin', '0', 750),
(6, 2, 2, 1, 1, 400, 400, '14:43:31', 'admin', '0', 1000),
(7, 3, 1, 30, 1, 50, 50, '14:43:42', 'admin', '0', 0),
(8, 4, 2, 31, 1, 70, 70, '14:43:48', 'admin', '0', 0),
(9, 2, 2, 31, 3, 70, 210, '02:45:11', 'admin', '0', 0),
(11, 6, 1, 31, 1, 70, 70, '14:56:52', 'admin', '0', 0),
(12, 7, 1, 30, 1, 50, 50, '14:56:59', 'admin', '0', 0),
(13, 8, 1, 31, 1, 70, 70, '14:57:22', 'admin', '1', 0),
(14, 9, 1, 35, 5, 130, 650, '02:57:34', 'admin', '1', 0),
(15, 10, 1, 1, 1, 40, 40, '15:02:03', 'admin', '0', 0),
(16, 11, 2, 1, 1, 40, 40, '15:05:17', 'admin', '0', 0),
(17, 11, 1, 30, 1, 50, 50, '15:05:25', 'admin', '0', 0),
(18, 11, 9, 30, 1, 50, 50, '15:05:32', 'admin', '1', 0),
(19, 12, 1, 42, 1, 40, 40, '15:20:54', 'admin', '0', 0),
(20, 13, 1, 31, 1, 70, 70, '15:21:15', 'admin', '0', 0),
(21, 14, 1, 33, 2, 30, 60, '03:21:53', 'admin', '0', 0),
(22, 15, 1, 32, 1, 35, 35, '15:22:54', 'admin', '0', 0),
(23, 16, 7, 31, 1, 70, 70, '15:23:36', 'admin', '0', 0),
(24, 17, 7, 31, 2, 70, 140, '03:26:11', 'admin', '0', 0),
(25, 17, 7, 31, 4, 70, 280, '03:26:11', 'admin', '0', 0),
(26, 18, 1, 31, 1, 70, 70, '03:35:22', 'admin', '0', 0),
(27, 19, 1, 31, 1, 70, 70, '15:39:49', 'admin', '0', 0),
(28, 20, 2, 32, 1, 35, 35, '15:41:43', 'admin', '0', 0),
(31, 23, 7, 31, 1, 70, 70, '16:20:30', 'admin', '0', 0),
(55, 24, 1, 35, 1, 130, 130, '16:39:34', 'admin', '0', 0),
(56, 24, 1, 39, 1, 60, 60, '16:47:05', 'admin', '0', 0),
(57, 24, 1, 45, 1, 35, 35, '16:47:06', 'admin', '0', 0),
(58, 25, 8, 31, 2, 70, 140, '04:47:14', 'admin', '0', 0),
(59, 25, 8, 33, 1, 30, 30, '16:47:14', 'admin', '0', 0),
(60, 25, 8, 1, 1, 40, 40, '16:47:15', 'admin', '0', 0),
(61, 25, 8, 30, 1, 50, 50, '16:47:15', 'admin', '0', 0),
(62, 25, 8, 35, 1, 130, 130, '16:47:16', 'admin', '0', 0),
(63, 26, 8, 32, 1, 35, 35, '16:47:19', 'admin', '0', 0),
(64, 27, 11, 32, 1, 35, 35, '16:47:22', 'admin', '0', 0),
(65, 27, 11, 31, 1, 70, 70, '16:47:22', 'admin', '0', 0),
(66, 27, 11, 33, 1, 30, 30, '16:47:22', 'admin', '0', 0),
(67, 28, 12, 31, 1, 70, 70, '16:47:32', 'admin', '0', 0),
(68, 28, 12, 1, 3, 40, 120, '04:47:33', 'admin', '0', 0),
(69, 28, 12, 33, 2, 30, 60, '04:47:34', 'admin', '0', 0),
(70, 28, 12, 31, 1, 70, 70, '16:47:35', 'admin', '0', 0),
(71, 29, 12, 37, 1, 120, 120, '16:48:35', 'admin', '0', 0),
(73, 30, 1, 35, 1, 130, 130, '09:29:16', 'admin', '0', 0),
(74, 30, 1, 37, 1, 120, 120, '09:29:17', 'admin', '0', 0),
(77, 31, 1, 31, 1, 70, 70, '10:21:01', 'admin', '0', 0),
(78, 32, 2, 31, 1, 70, 70, '10:21:10', 'admin', '0', 0),
(79, 32, 2, 33, 1, 30, 30, '10:21:12', 'admin', '0', 0),
(80, 33, 2, 31, 1, 70, 70, '10:26:03', 'admin', '0', 0),
(81, 34, 3, 1, 1, 40, 40, '10:26:07', 'admin', '0', 0),
(82, 34, 3, 31, 1, 70, 70, '10:26:08', 'admin', '1', 0),
(83, 35, 3, 35, 2, 130, 260, '10:26:26', 'admin', '0', 0),
(84, 35, 3, 39, 1, 60, 60, '10:26:27', 'admin', '0', 0),
(85, 35, 3, 38, 1, 170, 170, '10:26:29', 'admin', '0', 0),
(86, 35, 3, 37, 1, 120, 120, '10:26:29', 'admin', '0', 0),
(87, 36, 1, 39, 1, 60, 60, '10:26:37', 'admin', '0', 0),
(88, 37, 1, 32, 5, 35, 175, '10:26:45', 'admin', '0', 0),
(89, 38, 1, 33, 1, 30, 30, '10:26:52', 'admin', '0', 0),
(90, 39, 1, 33, 1, 30, 30, '10:28:56', 'admin', '0', 0),
(91, 40, 1, 31, 1, 70, 70, '10:30:35', 'admin', '0', 0),
(92, 40, 1, 33, 1, 30, 30, '10:30:36', 'admin', '0', 0),
(93, 41, 2, 1, 5, 40, 200, '10:30:41', 'admin', '0', 0),
(94, 42, 1, 35, 1, 130, 130, '10:36:43', 'admin', '0', 0),
(95, 43, 1, 33, 1, 30, 30, '10:36:45', 'admin', '0', 0),
(96, 44, 1, 30, 1, 50, 50, '10:36:48', 'admin', '0', 0),
(97, 45, 1, 33, 1, 30, 30, '10:36:50', 'admin', '0', 0),
(98, 46, 1, 34, 1, 40, 40, '10:36:52', 'admin', '0', 0),
(99, 47, 1, 1, 1, 40, 40, '10:58:13', 'admin', '0', 0),
(100, 48, 1, 1, 1, 40, 40, '10:58:17', 'admin', '0', 0),
(101, 48, 1, 1, 1, 40, 40, '10:59:02', 'admin', '0', 0),
(103, 50, 1, 36, 1, 100, 100, '10:59:50', 'admin', '0', 0),
(104, 50, 1, 41, 1, 90, 90, '10:59:51', 'admin', '0', 0),
(105, 50, 1, 39, 1, 60, 60, '10:59:52', 'admin', '0', 0),
(106, 50, 1, 38, 1, 170, 170, '10:59:53', 'admin', '0', 0),
(109, 33, 2, 32, 1, 35, 35, '11:30:50', 'admin', '0', 0),
(110, 51, 2, 32, 1, 35, 35, '11:33:18', 'admin', '0', 0),
(111, 52, 1, 31, 1, 70, 70, '11:33:24', 'admin', '0', 0),
(112, 53, 1, 33, 1, 30, 30, '12:52:04', 'admin', '0', 0),
(113, 53, 1, 31, 1, 70, 70, '12:52:05', 'admin', '0', 0),
(114, 53, 1, 32, 1, 35, 35, '12:52:06', 'admin', '0', 0),
(115, 53, 1, 1, 1, 40, 40, '12:52:07', 'admin', '0', 0),
(116, 54, 2, 35, 1, 130, 130, '12:52:15', 'admin', '0', 0),
(117, 54, 2, 36, 1, 100, 100, '12:52:17', 'admin', '0', 0),
(118, 54, 2, 33, 1, 30, 30, '12:52:17', 'admin', '0', 0),
(119, 55, 1, 1, 1, 40, 40, '12:53:06', 'admin', '0', 0),
(130, 56, NULL, 1, 1, 40, 40, '', '', '0', 0),
(131, 56, NULL, 31, 1, 70, 70, '', '', '0', 0),
(132, 56, NULL, 1, 1, 40, 40, '', '', '0', 0),
(136, 61, NULL, 50, 8, 100, 800, '', '', '0', 0),
(137, 57, NULL, 39, 1, 60, 60, '', '', '0', 0),
(141, 5, NULL, 1, 5, 40, 200, '', '', '0', 0),
(142, 60, NULL, 30, 10, 50, 500, '', '', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bill_item_master_original`
--

CREATE TABLE `bill_item_master_original` (
  `bim_id` int(10) NOT NULL,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_item_master_original`
--

INSERT INTO `bill_item_master_original` (`bim_id`, `billid`, `bim_itemid`, `bim_item_quantity`, `bim_item_quantity_price`, `bim_item_price`) VALUES
(1, 6137, 57, 2, 85, 170),
(2, 57, 43, 2, 20, 40),
(3, 57, 42, 2, 40, 80),
(4, 58, 39, 5, 60, 300),
(5, 59, 50, 20, 100, 2000),
(6, 60, 30, 10, 50, 500),
(7, 61, 50, 8, 100, 800);

-- --------------------------------------------------------

--
-- Table structure for table `bill_master`
--

CREATE TABLE `bill_master` (
  `billid` int(11) NOT NULL,
  `bill_name` varchar(100) DEFAULT NULL,
  `bill_mobile` varchar(50) DEFAULT NULL,
  `bill_table` varchar(100) DEFAULT NULL,
  `bill_waiter` varchar(100) DEFAULT NULL,
  `table_part` varchar(5) DEFAULT NULL,
  `bill_status` varchar(25) DEFAULT NULL,
  `bill_notes` varchar(255) DEFAULT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL DEFAULT '0',
  `t_status` enum('L','C','R') NOT NULL,
  `update_time` time NOT NULL,
  `isvisible` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_master`
--

INSERT INTO `bill_master` (`billid`, `bill_name`, `bill_mobile`, `bill_table`, `bill_waiter`, `table_part`, `bill_status`, `bill_notes`, `bill_date`, `bill_time`, `bill_total`, `t_status`, `update_time`, `isvisible`) VALUES
(2, NULL, NULL, '42', '2', 'A', NULL, NULL, '2017-04-22 14:49:30', '14:41:59', 1470, 'C', '02:45:11', '0'),
(3, NULL, NULL, '51', '1', 'D', NULL, NULL, '2017-04-22 14:45:36', '14:43:42', 50, 'C', '14:43:42', '0'),
(4, NULL, NULL, '52', '2', 'D', NULL, NULL, '2017-04-22 14:43:53', '14:43:48', 70, 'C', '14:43:48', '0'),
(5, '', '', '52', '2', 'D', 'By Card', '', '2017-04-22 15:45:04', '14:45:26', 200, 'C', '02:45:31', '0'),
(6, NULL, NULL, '101', '1', 'C', NULL, NULL, '2017-04-22 15:01:26', '14:56:52', 70, 'C', '14:56:52', '0'),
(7, NULL, NULL, '102', '1', 'C', NULL, NULL, '2017-04-22 14:57:04', '14:56:59', 50, 'C', '14:56:59', '0'),
(8, NULL, NULL, '2', '1', 'A', NULL, NULL, '2017-04-22 15:08:32', '14:57:22', 70, 'R', '14:57:22', '1'),
(9, NULL, NULL, '2', '1', 'B', NULL, NULL, '2017-04-22 14:57:42', '14:57:28', 650, 'C', '02:57:34', '0'),
(10, NULL, NULL, '102', '1', 'C', NULL, NULL, '2017-04-22 15:02:07', '15:02:03', 40, 'C', '15:02:03', '0'),
(11, NULL, NULL, '4', '9', 'A', NULL, NULL, '2017-04-22 15:08:05', '15:05:17', 140, 'C', '15:05:32', '0'),
(12, NULL, NULL, '42', '1', 'A', NULL, NULL, '2017-04-22 15:44:03', '15:20:54', 40, 'C', '15:20:54', '0'),
(13, NULL, NULL, '102', '1', 'C', NULL, NULL, '2017-04-22 15:21:30', '15:21:15', 70, 'C', '15:21:15', '0'),
(14, NULL, NULL, '102', '1', 'C', NULL, NULL, '2017-04-22 15:22:03', '15:21:50', 60, 'C', '03:21:53', '0'),
(15, NULL, NULL, '102', '1', 'C', NULL, NULL, '2017-04-22 15:43:49', '15:22:54', 35, 'C', '15:22:54', '0'),
(16, NULL, NULL, '103', '7', 'C', NULL, NULL, '2017-04-22 15:25:50', '15:23:36', 70, 'C', '15:23:36', '0'),
(17, NULL, NULL, '103', '7', 'C', NULL, NULL, '2017-04-22 15:44:52', '15:25:58', 420, 'C', '03:26:11', '0'),
(18, NULL, NULL, '2', '1', 'A', NULL, NULL, '2017-04-22 15:44:29', '15:35:18', 70, 'C', '03:35:22', '0'),
(19, NULL, NULL, '1', '1', 'A', NULL, NULL, '2017-04-22 15:43:42', '15:39:49', 70, 'C', '15:39:49', '0'),
(20, NULL, NULL, '1', '2', 'B', NULL, NULL, '2017-04-22 15:43:33', '15:41:43', 35, 'C', '15:41:43', '0'),
(23, NULL, NULL, '103', '7', 'C', NULL, NULL, '2017-04-24 12:09:25', '16:20:30', 70, 'C', '16:20:30', '0'),
(24, NULL, NULL, '4', '1', 'A', NULL, NULL, '2017-04-24 16:47:53', '16:39:34', 225, 'C', '16:47:06', '0'),
(25, NULL, NULL, '3', '8', 'A', NULL, NULL, '2017-04-24 16:47:49', '16:47:13', 390, 'C', '16:47:16', '0'),
(26, NULL, NULL, '1', '8', 'A', NULL, NULL, '2017-04-24 16:47:45', '16:47:19', 35, 'C', '16:47:19', '0'),
(27, NULL, NULL, '2', '11', 'A', NULL, NULL, '2017-04-24 16:47:42', '16:47:22', 135, 'C', '16:47:22', '0'),
(28, NULL, NULL, '7', '12', 'A', NULL, NULL, '2017-04-24 16:47:37', '16:47:32', 320, 'C', '16:47:35', '0'),
(29, NULL, NULL, '7', '12', 'A', NULL, NULL, '2017-04-24 16:48:37', '16:48:35', 120, 'C', '16:48:35', '0'),
(30, NULL, NULL, '2', '1', 'A', NULL, NULL, '2017-04-25 10:21:29', '09:29:16', 250, 'C', '09:33:26', '0'),
(31, NULL, NULL, '1', '1', 'A', NULL, NULL, '2017-04-25 10:21:34', '10:21:01', 70, 'C', '10:21:01', '0'),
(32, NULL, NULL, '3', '2', 'A', NULL, NULL, '2017-04-25 10:21:15', '10:21:10', 100, 'C', '10:21:12', '0'),
(33, NULL, NULL, '3', '2', 'A', NULL, NULL, '2017-04-25 11:31:24', '10:26:03', 105, 'C', '11:30:50', '0'),
(34, NULL, NULL, '4', '3', 'A', NULL, NULL, '2017-04-25 10:26:15', '10:26:07', 110, 'C', '10:26:08', '0'),
(35, NULL, NULL, '4', '3', 'A', NULL, NULL, '2017-04-25 11:30:59', '10:26:25', 610, 'C', '10:26:29', '0'),
(36, NULL, NULL, '103', '1', 'C', NULL, NULL, '2017-04-25 10:27:04', '10:26:37', 60, 'C', '10:26:37', '0'),
(37, NULL, NULL, '104', '1', 'C', NULL, NULL, '2017-04-25 10:27:00', '10:26:44', 175, 'C', '10:26:45', '0'),
(38, NULL, NULL, '105', '1', 'C', NULL, NULL, '2017-04-25 10:26:55', '10:26:52', 30, 'C', '10:26:52', '0'),
(39, NULL, NULL, '1', '1', 'A', NULL, NULL, '2017-04-25 10:31:08', '10:28:56', 30, 'C', '10:28:56', '0'),
(40, NULL, NULL, '51', '1', 'D', NULL, NULL, '2017-04-25 10:30:35', '10:30:35', 100, 'L', '10:30:36', '1'),
(41, NULL, NULL, '52', '2', 'D', NULL, NULL, '2017-04-25 10:30:57', '10:30:39', 200, 'C', '10:30:41', '0'),
(42, NULL, NULL, '105', '1', 'C', NULL, NULL, '2017-04-25 16:45:46', '10:36:43', 130, 'C', '10:36:43', '0'),
(43, 'as', NULL, '106', '1', 'C', NULL, NULL, '2017-04-25 10:37:06', '10:36:45', 30, 'C', '10:36:45', '0'),
(44, NULL, NULL, '107', '1', 'C', NULL, NULL, '2017-04-25 10:37:02', '10:36:48', 50, 'C', '10:36:48', '0'),
(45, NULL, NULL, '108', '1', 'C', NULL, NULL, '2017-04-25 10:36:58', '10:36:50', 30, 'C', '10:36:50', '0'),
(46, NULL, NULL, '109', '1', 'C', NULL, NULL, '2017-04-25 10:36:54', '10:36:52', 40, 'C', '10:36:52', '0'),
(47, NULL, NULL, '1', '1', 'A', NULL, NULL, '2017-04-25 10:58:26', '10:58:13', 40, 'C', '10:58:13', '0'),
(48, NULL, NULL, '2', '1', 'A', NULL, NULL, '2017-04-25 10:59:16', '10:58:17', 80, 'C', '10:59:02', '0'),
(50, NULL, NULL, '1', '1', 'A', NULL, NULL, '2017-04-25 11:29:48', '10:59:50', 420, 'C', '11:23:09', '0'),
(51, NULL, NULL, '3', '2', 'A', NULL, NULL, '2017-04-25 11:33:31', '11:33:18', 35, 'C', '11:33:18', '0'),
(52, NULL, NULL, '1', '1', 'A', NULL, NULL, '2017-04-25 12:51:51', '11:33:24', 70, 'C', '11:33:24', '0'),
(53, NULL, NULL, '2', '1', 'A', NULL, NULL, '2017-04-25 12:52:49', '12:52:04', 175, 'C', '12:52:07', '0'),
(54, NULL, NULL, '1', '2', 'A', NULL, NULL, '2017-04-25 12:52:38', '12:52:15', 260, 'C', '12:52:17', '0'),
(55, NULL, NULL, '2', '1', 'A', NULL, NULL, '2017-04-25 16:45:37', '12:53:06', 40, 'C', '12:53:06', '0'),
(56, '', '', '1', '1', 'A', 'nottaken', '', '2017-04-25 13:59:30', '13:59:18', 150, 'C', '13:59:26', '0'),
(57, '', '', '1', '1', 'A', 'nottaken', '', '2017-04-25 17:38:36', '17:38:32', 60, 'C', '17:38:32', '1'),
(60, '', '', '12', 'N', NULL, 'By Card', '', '2017-04-26 08:53:18', '08:53:18', 500, 'L', '00:00:00', '1'),
(61, 'Nikunj', '123456879', '5', 'B', NULL, 'By Card', '', '2017-04-26 09:29:22', '09:29:22', 800, 'L', '00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `bill_master_original`
--

CREATE TABLE `bill_master_original` (
  `billid` int(11) NOT NULL,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` int(10) NOT NULL,
  `bill_waiter` int(10) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(10) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `itemid` int(10) NOT NULL,
  `itemname` varchar(100) NOT NULL,
  `kgPrice` varchar(255) NOT NULL,
  `price_type` varchar(255) NOT NULL,
  `itemSeq` int(11) DEFAULT NULL,
  `backColor` varchar(255) NOT NULL,
  `visible` varchar(255) NOT NULL,
  `itemprice` int(11) NOT NULL,
  `itemnotes` varchar(255) NOT NULL,
  `item_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`itemid`, `itemname`, `kgPrice`, `price_type`, `itemSeq`, `backColor`, `visible`, `itemprice`, `itemnotes`, `item_image`) VALUES
(1, 'Vanilla', '400', 'setIceCreamItemId', 1, '#d2a8ea', '0', 40, '', '1_1vanilla-bean-ice-cream-2-mark.jpeg'),
(30, 'Ice Cream', '500', 'setIceCreamItemId', 2, '#c0a092', '0', 50, '', '30_44chocolate-mint-stout-ice-cream-tk.jpeg'),
(31, 'Shiv ice cream', '700', 'setIceCreamItemId', 3, '#b1b1b1', '1', 70, '', '31_311.jpg'),
(32, 'Kingcone', '0', 'setItemId', 4, '#9ebdde', '1', 35, '', '32_32kingcone.jpg'),
(33, 'Candy', '0', 'setItemId', 5, '#91e6ec', '1', 30, '', '33_33candy.jpg'),
(34, 'Mava candy', '0', 'setItemId', 6, '#c3c3c3', '1', 40, '', '34_34mava-candy.jpeg'),
(35, 'Sundae', '0', 'setItemId', 7, '#d3625f', '1', 130, '', '35_35sundae.jpeg'),
(36, 'Vanila/Coffee', '0', 'setItemId', 8, '#cbbcb8', '1', 100, '', '36_36cookie-dough-vanilla-milk-shake-simplygloria.com-.jpeg'),
(37, 'Shake', '0', 'setItemId', 9, '#9fc4c4', '1', 120, '', '37_37chocolate-shake2.jpeg'),
(38, 'Shiv sp. Shake', '0', 'setItemId', 10, '#9bcdff', '1', 170, '', '38_38vanilacoffeesp.jpg'),
(39, 'Chickoo shake', '0', 'setItemId', 11, '#b4988b', '1', 60, '', '39_39chikoo-2.jpeg'),
(40, 'Juice', '', 'setIceCreamItemId', 55, '#a2a2a2', '0', 60, '', '40_40download-1.jpg'),
(41, 'Faluda', '0', 'setItemId', 13, '#fdffbb', '1', 90, '', '41_41faluda23.jpg'),
(42, 'Extra Topping', '0', 'setItemId', 14, '#ffa6a8', '1', 40, '', '42_42extopping.jpg'),
(43, 'Half Vanila', '0', 'setItemId', 15, '#c6caff', '1', 20, '', '43_1vanilla-bean-ice-cream-2-mark.jpeg'),
(44, 'Half Ice cream', '0', 'setItemId', 16, '#c1a8a8', '1', 25, '', '44_44chocolate-mint-stout-ice-cream-tk.jpeg'),
(45, 'Half Shiv ice cream', '0', 'setItemId', 17, '#a8bbba', '1', 35, '', '45_311.jpg'),
(46, 'Cold Drink', '0', 'setItemId', 18, '#9ea9d1', '1', 30, '', '46_46coldrink.jpeg'),
(47, 'Min.Water', '0', 'setItemId', 20, '#9fcccc', '1', 20, '', '47_47bisleri1---copy.jpg'),
(48, 'French Fries', '0', 'setItemId', 20, '#f1e9ad', '1', 80, '', '48_48french-fries-1.jpeg'),
(49, 'Chutney', '0', 'setItemId', 21, '#b0d9aa', '1', 10, '', '49_491.jpeg'),
(50, 'Grill', '0', 'setItemId', 22, '#e8bdec', '1', 100, '', '50_50full-sandwich.jpg'),
(51, 'Non Grill', '0', 'setItemId', 23, '#b0a9b4', '1', 95, '', '51_51veg-grill-sandwich-big.jpg'),
(52, 'Burger', '0', 'setItemId', 24, '#dac5da', '1', 90, '', '52_52burger.jpeg'),
(53, 'Club', '0', 'setItemId', 25, '#ede7b8', '1', 150, '', '53_53club.jpg'),
(54, 'Oven Toast', '0', 'setItemId', 26, '#afedd5', '1', 150, '', '54_54soupssaladssandwichescreamcentrelarge.jpeg'),
(55, 'Pizza', '0', 'setItemId', 27, '#bcdb9d', '1', 170, '', '55_55pizzas.jpg'),
(56, 'Shiv Pizza', '0', 'setItemId', 28, '#ffb9bb', '1', 225, '', '56_56shiv-pizza.jpg'),
(57, 'Bread Grill', '0', 'setItemId', 29, '#c5e9fa', '1', 85, '', '57_57breadbuttergril.jpg'),
(58, 'Bread Non Grill', '0', 'setItemId', 30, '#cee0ff', '1', 80, '', '58_58bread-butter-1171149.jpeg'),
(59, 'Garlic Bread', '0', 'setItemId', 31, '#fff1bf', '1', 95, '', '59_59garlicbread.jpeg'),
(60, 'Rice/Bhel', '0', 'setItemId', 32, '#e89fa2', '1', 140, '', '60_60ricebhel.jpeg'),
(61, 'Noodles', '0', 'setItemId', 33, '#d9cab7', '1', 140, '', '61_61schezwan-veg-noodles.jpeg'),
(62, 'M.Dry/Gravy', '0', 'setItemId', 34, '#b3bde1', '1', 140, '', '62_62manchurian-dry.jpeg'),
(63, 'Trip. Sch. Rice', '0', 'setItemId', 35, '#c6e1b5', '1', 220, '', '63_63trip-rice.jpeg'),
(64, 'DF. Faluda', '0', 'setItemId', 37, '#fffec4', '1', 100, '', '64_64faluda23.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `siteadmin`
--

CREATE TABLE `siteadmin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `adminemail` varchar(50) NOT NULL,
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siteadmin`
--

INSERT INTO `siteadmin` (`admin_id`, `username`, `password`, `fullname`, `adminemail`, `user_type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'shive admin', 'admin@gmail.com', 1),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', '', 'user@gmail.com', 2),
(3, 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', '', 'superadmin@gmail.com', 3),
(4, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', '', 'user1@gmail.com', 2),
(5, 'user2', '7e58d63b60197ceb55a1c487989a3720', '', 'user2@gmail.com', 2),
(6, 'user3', '92877af70a45fd6a2ed7fe81e1236b78', '', 'user3@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_master`
--

CREATE TABLE `table_master` (
  `tid` int(11) NOT NULL,
  `tname` varchar(20) NOT NULL,
  `backColor` varchar(250) NOT NULL,
  `floor` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_master`
--

INSERT INTO `table_master` (`tid`, `tname`, `backColor`, `floor`) VALUES
(3, '1', '', '0'),
(4, '2', '', '0'),
(5, '3', '', '0'),
(6, '4', '', '0'),
(7, '5', '', '0'),
(8, '6', '', '0'),
(9, '7', '', '0'),
(10, '8', '', '0'),
(11, '9', '', '0'),
(12, '10', '', '0'),
(13, '11', '', '0'),
(14, '12', '', '0'),
(15, '13', '', '0'),
(16, '14', '', '0'),
(17, '15', '', '0'),
(18, '16', '', '0'),
(19, '17', '', '0'),
(20, '18', '', '0'),
(21, '19', '', '0'),
(22, '20', '', '0'),
(23, '51', '#7382e8', '1'),
(24, '52', '', '1'),
(25, '53', '', '1'),
(26, '54', '', '1'),
(27, '55', '', '1'),
(28, '56', '', '1'),
(29, '57', '', '1'),
(30, '58', '', '1'),
(31, '59', '', '1'),
(32, '60', '', '1'),
(35, 'NJ', '#ff0080', '0'),
(36, 'NIK', '#ff8000', '0'),
(37, '42', '#808000', '0');

-- --------------------------------------------------------

--
-- Table structure for table `temp_bill_item_master`
--

CREATE TABLE `temp_bill_item_master` (
  `bim_id` int(10) NOT NULL,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` int(11) NOT NULL,
  `parcel` int(11) NOT NULL,
  `weight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_bill_item_master`
--

INSERT INTO `temp_bill_item_master` (`bim_id`, `billid`, `bim_itemid`, `bim_item_quantity`, `bim_item_quantity_price`, `bim_item_price`, `parcel`, `weight`) VALUES
(8, 2, 35, 5, 130, 650, 1, 0),
(45, 3, 1, 1, 40, 40, 0, 0),
(46, 3, 31, 1, 70, 70, 0, 0),
(47, 3, 1, 1, 40, 40, 0, 0),
(48, 3, 40, 1, 60, 60, 0, 0),
(49, 3, 46, 2, 30, 60, 0, 0),
(65, 1, 1, 5, 40, 200, 0, 0),
(66, 1, 33, 2, 30, 60, 0, 0),
(67, 1, 1, 1, 40, 40, 0, 0),
(68, 1, 1, 1, 40, 40, 0, 0),
(69, 1, 1, 1, 40, 40, 0, 0),
(70, 1, 1, 1, 40, 40, 0, 0),
(71, 1, 31, 3, 70, 210, 0, 0),
(72, 1, 64, 5, 100, 500, 0, 0),
(73, 1, 40, 2, 60, 120, 0, 0),
(74, 4, 43, 2, 20, 40, 0, 0),
(75, 4, 42, 2, 40, 80, 0, 0),
(76, 5, 43, 2, 20, 40, 0, 0),
(77, 5, 42, 2, 40, 80, 0, 0),
(79, 7, 50, 20, 100, 2000, 0, 0),
(80, 8, 50, 20, 100, 2000, 0, 0),
(81, 9, 39, 1, 60, 60, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp_bill_master`
--

CREATE TABLE `temp_bill_master` (
  `billid` int(11) NOT NULL,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` int(10) NOT NULL,
  `bill_waiter` int(10) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_bill_master`
--

INSERT INTO `temp_bill_master` (`billid`, `bill_name`, `bill_mobile`, `bill_table`, `bill_waiter`, `bill_status`, `bill_notes`, `bill_date`, `bill_time`, `bill_total`) VALUES
(1, '', '', 42, 2, 'nottaken', '', '2017-04-22 14:49:30', '14:41:59', 1250),
(2, '', '', 2, 1, 'nottaken', '', '2017-04-22 14:57:42', '14:57:28', 650),
(3, '', '', 1, 1, 'nottaken', '', '2017-04-25 13:59:30', '13:59:18', 270),
(4, '', '', 2, 0, 'nottaken', '', '2017-04-25 17:32:09', '17:32:09', 120),
(5, '', '', 2, 0, 'nottaken', '', '2017-04-25 17:32:09', '17:32:09', 120),
(7, '', '9998258360', 0, 5, 'nottaken', '', '2017-04-25 17:46:31', '17:46:31', 2000),
(8, '', '9998258360', 0, 5, 'nottaken', '', '2017-04-25 17:46:31', '17:46:31', 2000),
(9, '', '', 1, 1, 'nottaken', '', '2017-04-25 17:38:36', '17:38:32', 60);

-- --------------------------------------------------------

--
-- Table structure for table `waiter_master`
--

CREATE TABLE `waiter_master` (
  `waiter_id` int(11) NOT NULL,
  `waiter` varchar(20) NOT NULL,
  `floor` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `waiter_master`
--

INSERT INTO `waiter_master` (`waiter_id`, `waiter`, `floor`) VALUES
(1, 'T', '0'),
(2, 'N', '0'),
(3, 'G', '0'),
(4, 'A', '0'),
(5, 'J', '0'),
(6, 'D', '0'),
(7, 'V', '0'),
(8, 'B', '0'),
(9, 'F', '0'),
(10, 'O', '0'),
(11, 'M', '0'),
(12, 'L', '0'),
(13, 'U', '0'),
(14, 'H', '0'),
(15, 'K', '0'),
(16, 'P', '0'),
(17, 'FA', '1'),
(18, 'FB', '1'),
(19, 'FC', '1'),
(20, 'FD', '1'),
(21, 'FE', '1'),
(22, 'FF', '1'),
(23, 'FG', '1'),
(24, 'FH', '1'),
(25, 'FI', '1'),
(26, 'FJ', '1'),
(27, 'FK', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billdetail`
--
ALTER TABLE `billdetail`
  ADD PRIMARY KEY (`billdetailid`);

--
-- Indexes for table `bill_item_master`
--
ALTER TABLE `bill_item_master`
  ADD PRIMARY KEY (`bim_id`);

--
-- Indexes for table `bill_item_master_original`
--
ALTER TABLE `bill_item_master_original`
  ADD PRIMARY KEY (`bim_id`);

--
-- Indexes for table `bill_master`
--
ALTER TABLE `bill_master`
  ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `bill_master_original`
--
ALTER TABLE `bill_master_original`
  ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`itemid`);

--
-- Indexes for table `siteadmin`
--
ALTER TABLE `siteadmin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `table_master`
--
ALTER TABLE `table_master`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `temp_bill_item_master`
--
ALTER TABLE `temp_bill_item_master`
  ADD PRIMARY KEY (`bim_id`);

--
-- Indexes for table `temp_bill_master`
--
ALTER TABLE `temp_bill_master`
  ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `waiter_master`
--
ALTER TABLE `waiter_master`
  ADD PRIMARY KEY (`waiter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billdetail`
--
ALTER TABLE `billdetail`
  MODIFY `billdetailid` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bill_item_master`
--
ALTER TABLE `bill_item_master`
  MODIFY `bim_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `bill_item_master_original`
--
ALTER TABLE `bill_item_master_original`
  MODIFY `bim_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bill_master`
--
ALTER TABLE `bill_master`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `bill_master_original`
--
ALTER TABLE `bill_master_original`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_master`
--
ALTER TABLE `item_master`
  MODIFY `itemid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `siteadmin`
--
ALTER TABLE `siteadmin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `table_master`
--
ALTER TABLE `table_master`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `temp_bill_item_master`
--
ALTER TABLE `temp_bill_item_master`
  MODIFY `bim_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `temp_bill_master`
--
ALTER TABLE `temp_bill_master`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `waiter_master`
--
ALTER TABLE `waiter_master`
  MODIFY `waiter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `temp_bill_item_master` ADD `parcel` VARCHAR(2) NOT NULL DEFAULT '0' AFTER `bim_item_price`;
ALTER TABLE `temp_bill_master` ADD `table_part` VARCHAR(5) NOT NULL AFTER `bill_waiter`;
ALTER TABLE `temp_bill_master` CHANGE `table_part` `table_part` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
